#ifndef QCLABEL_H
#define QCLABEL_H

// Define a type alias for QLabel pointer
typedef void* QCLabel;

#ifdef __cplusplus
extern "C" {
#endif

// Create a QLabel instance
QCLabel qlabel_new(const char *text);

// Set text for the QLabel instance
void qlabel_set_text(QCLabel label, const char *text);

// Get text from the QLabel instance
const char* qlabel_get_text(QCLabel label);

// Destroy the QLabel instance and clean up resources
void qlabel_destroy(QCLabel label);

#ifdef __cplusplus
}
#endif

#endif // QLABEL_H
