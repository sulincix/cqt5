#include <QLabel>

extern "C" {
#include "QCLabel.h"
}

// Create a QLabel instance
QCLabel qlabel_new(const char *text) {
    QLabel* label = new QLabel(text);
    return (QCLabel)label;
}

// Set text for the QLabel instance
void qlabel_set_text(QCLabel label, const char *text) {
    QLabel* cLabel = (QLabel*)label;
    if (cLabel != nullptr) {
        cLabel->setText(text);
    }
}

// Get text from the QLabel instance
const char* qlabel_get_text(QCLabel label) {
    QLabel* cLabel = (QLabel*)label;
    if (cLabel != nullptr) {
        return cLabel->text().toUtf8().constData();
    }
    return nullptr;
}

// Destroy the QLabel instance and clean up resources
void qlabel_destroy(QCLabel label) {
    QLabel* cLabel = (QLabel*)label;
    if (cLabel != nullptr) {
        delete cLabel;
    }
}
