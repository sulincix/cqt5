#include <QApplication>

extern "C" {
#include "QCApplication.h"
}

// Create a QApplication instance
QCApplication qapplication_new(int argc, char *argv[]) {
    QApplication* app = new QApplication(argc, argv);
    return (QCApplication)app;
}

// Run the application event loop
void qapplication_exec(QCApplication app) {
    QApplication* cApp = (QApplication*)app;
    cApp->exec();
}

// Destroy the QApplication instance and clean up resources
void qapplication_destroy(QCApplication app) {
    QApplication* cApp = (QApplication*)app;
    if (cApp != nullptr) {
        delete cApp;
    }
}
